import React from 'react'

export default function ProductCategoryRow(props) {
  const category = props.category;
  return (
    <div>
      <tr>
        <th colSpan="2">{category}</th>
      </tr>
    </div>
  )
}
